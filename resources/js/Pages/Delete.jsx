import FormDeleteMemberComp from '@/CustomComponents/FormDeleteMemberComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Delete({ auth, user }) {
    return (
        <>
            <Head title="Delete Anggota Perpustakaan" />
            <NavbarLayout auth={auth.user} />

            <FormDeleteMemberComp data={user} />
        </>
    );
}
