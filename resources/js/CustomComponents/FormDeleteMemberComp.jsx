import { Link, useForm } from '@inertiajs/react'
import React from 'react'

const FormDeleteMemberComp = ({ data }) => {
    const { delete: destroy, processing } = useForm({ });

    const deleteUser = () => {
        destroy(route('delete.anggota.process', data.id), { });
    };

    return (
        <>
            <div className="max-w-[85rem] px-4 py-4 sm:px-6 lg:px-8 lg:py-14 mx-auto">
                <div className="mx-auto max-w-2xl">
                    <div className="text-center">
                        <h2 className="text-xl text-gray-800 font-bold sm:text-3xl dark:text-white">
                            Delete Member ID: {data.id}
                        </h2>
                    </div>

                    <div className="mt-5 p-4 relative z-10 bg-white border rounded-xl sm:mt-10 md:p-10 dark:bg-gray-800 dark:border-gray-700">
                        <div className="mb-4 sm:mb-8">
                            <label
                                htmlFor="hs-feedback-post-comment-name-1"
                                className="block mb-2 text-sm font-medium dark:text-white"
                            >
                                Name
                            </label>
                            <input
                                disabled
                                defaultValue={data.name}
                                type="text"
                                id="hs-feedback-post-comment-name-1"
                                className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                placeholder="name"
                            />
                        </div>
                        <div className="mb-4 sm:mb-8">
                            <label
                                htmlFor="hs-feedback-post-comment-email-1"
                                className="block mb-2 text-sm font-medium dark:text-white"
                            >
                                Email
                            </label>
                            <input
                                disabled
                                defaultValue={data.email}
                                type="email"
                                id="hs-feedback-post-comment-email-1"
                                className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                placeholder="email"
                            />
                        </div>
                        <div className="mb-4 sm:mb-8">
                            <label
                                htmlFor="hs-feedback-post-comment-email-1"
                                className="block mb-2 text-sm font-medium dark:text-white"
                            >
                                Phone Number
                            </label>
                            <input
                                disabled
                                defaultValue={data.phone_number}
                                type="number"
                                id="hs-feedback-post-comment-email-1"
                                className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                placeholder="phone number"
                            />
                        </div>
                        <div className="mb-2 sm:mb-8">
                            <label
                                htmlFor="hs-feedback-post-comment-email-1"
                                className="block mb-2 text-sm font-medium dark:text-white"
                            >
                                Address
                            </label>
                            <textarea
                                disabled
                                defaultValue={data.address}
                                id="hs-feedback-post-comment-textarea-1"
                                name="hs-feedback-post-comment-textarea-1"
                                rows={3}
                                className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                placeholder="address"
                            />
                        </div>
                        <div className="-mt-3 sm:mb-8">
                            <label
                                htmlFor="af-submit-app-category"
                                className="inline-block text-sm font-medium text-gray-800 mt-2.5 dark:text-gray-200"
                            >
                                Role
                            </label>
                            <select
                                disabled
                                value={data?.role}
                                className="py-2 px-3 pe-9 block w-full border-gray-200 shadow-sm rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                            >
                                <option value="Admin">Admin</option>
                                <option value="Member">Member</option>
                            </select>
                        </div>


                        <div className="mt-6 grid">
                            <button
                                onClick={() => deleteUser()}
                                className="w-full py-2 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-red-600 text-white hover:bg-red-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                            >
                                Delete
                            </button>
                            <Link
                                href={route('master.anggota')}
                                className="w-full mt-4 py-1 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-gray-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                            >
                                back
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FormDeleteMemberComp