import { Link } from '@inertiajs/react'
import moment from 'moment/moment'
import React from 'react'

const MasterTableComp = ({ session, data }) => {
    return (
        <>
            <div className="max-w-[85rem] px-4 py-10 sm:px-6 lg:px-8 lg:py-14 mx-auto">
                <div className="flex flex-col">
                    <div className="-m-1.5 overflow-x-auto">

                        {
                            session === "user_created" || session === "user_updated" || session === "user_deleted" ?
                                <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
                                    <span class="font-medium">Success!</span><br />
                                    {session === "user_created" ? "User Created" : session === "user_updated" ? "User Updated" : "User Deleted"}
                                </div> : null
                        }

                        <div className="p-1.5 min-w-full inline-block align-middle">
                            <div className="bg-white border border-gray-200 rounded-xl shadow-sm overflow-hidden dark:bg-slate-900 dark:border-gray-700">
                                <div className="px-6 py-4 grid gap-3 md:flex md:justify-between md:items-center border-b border-gray-200 dark:border-gray-700">
                                    <div>
                                        <h2 className="text-xl font-semibold text-gray-800 dark:text-gray-200">
                                            Groups
                                        </h2>
                                        <p className="text-sm text-gray-600 dark:text-gray-400">
                                            Kslola semua anggota perpustakaan
                                        </p>
                                    </div>
                                    <div>
                                        <div className="inline-flex gap-x-2">
                                            <Link
                                                className="py-1 px-2 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                                                href={route('kelola.anggota', {
                                                    id: null,
                                                    type: "create"
                                                })}
                                            >
                                                <svg
                                                    className="flex-shrink-0 w-3 h-3"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    width={16}
                                                    height={16}
                                                    viewBox="0 0 16 16"
                                                    fill="none"
                                                >
                                                    <path
                                                        d="M2.63452 7.50001L13.6345 7.5M8.13452 13V2"
                                                        stroke="currentColor"
                                                        strokeWidth={2}
                                                        strokeLinecap="round"
                                                    />
                                                </svg>
                                                Add Member
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                {/* Table */}
                                <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                                    <thead className="bg-gray-50 dark:bg-slate-800">
                                        <tr>
                                            <th
                                                scope="col"
                                                className="ps-6 pe-6 py-3 text-start"
                                            >
                                                <div className="flex items-center gap-x-2">
                                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-800 dark:text-gray-200">
                                                        ID
                                                    </span>
                                                </div>
                                            </th>
                                            <th
                                                scope="col"
                                                className="ps-6 pe-6 py-3 text-start"
                                            >
                                                <div className="flex items-center gap-x-2">
                                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-800 dark:text-gray-200">
                                                        Nama
                                                    </span>
                                                </div>
                                            </th>
                                            <th scope="col" className="px-6 py-3 text-start">
                                                <div className="flex items-center gap-x-2">
                                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-800 dark:text-gray-200">
                                                        Email
                                                    </span>
                                                </div>
                                            </th>
                                            <th scope="col" className="px-6 py-3 text-start">
                                                <div className="flex items-center gap-x-2">
                                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-800 dark:text-gray-200">
                                                        Alamat
                                                    </span>
                                                </div>
                                            </th>
                                            <th scope="col" className="px-6 py-3 text-start">
                                                <div className="flex items-center gap-x-2">
                                                    <span className="text-xs font-semibold uppercase tracking-wide text-gray-800 dark:text-gray-200">
                                                        Registered
                                                    </span>
                                                </div>
                                            </th>
                                            <th scope="col" className="px-6 py-3 text-end" />
                                        </tr>
                                    </thead>
                                    <tbody className="divide-y divide-gray-200 dark:divide-gray-700">
                                        {
                                            data?.length > 0 ? data.map((item, key) => {
                                                return (
                                                    <tr key={key + 1}>
                                                        <td className="h-px w-2 whitespace-nowrap">
                                                            <div className="ml-4 px-2 py-2">
                                                                <Link
                                                                    href={route('kelola.anggota', {
                                                                        id: item?.id,
                                                                        type: "detail"
                                                                    })}
                                                                >
                                                                    <button
                                                                        className="w-md py-1 px-3 inline-flex justify-center items-center gap-x-2 text-gray text-sm font-semibold rounded-lg border border-blue-600 hover:text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                                                                    >
                                                                        {item?.id}
                                                                    </button>
                                                                </Link>
                                                            </div>
                                                        </td>
                                                        <td className="h-px w-px whitespace-nowrap">
                                                            <div className="ps-6 pe-16 py-3">
                                                                <div className="flex items-center gap-x-3">
                                                                    <img
                                                                        className="inline-block h-[2.375rem] w-[2.375rem] rounded-full"
                                                                        src="https://images.unsplash.com/photo-1531927557220-a9e23c1e4794?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=facearea&facepad=2&w=300&h=300&q=80"
                                                                        alt="Image Description"
                                                                    />
                                                                    <div className="grow">
                                                                        <span className="block text-sm font-semibold text-gray-800 dark:text-gray-200">
                                                                            {item?.name}
                                                                        </span>
                                                                        <span className="block text-sm text-gray-500">
                                                                            {item?.role}
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className="h-px w-72 whitespace-nowrap">
                                                            <div className="px-6 py-3">
                                                                <span className="block text-sm font-semibold text-gray-800 dark:text-gray-200">
                                                                    {item?.email}
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td className="h-px w-px whitespace-nowrap">
                                                            <div className="px-6 py-3">
                                                                <span className="block text-sm font-semibold text-gray-800 dark:text-gray-200">
                                                                    {item?.address}
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td className="h-px w-px whitespace-nowrap">
                                                            <div className="px-6 py-3">
                                                                <span className="text-sm text-gray-500">
                                                                    {moment(item?.created_at).format("DD MMM YYYY, H:mm")}
                                                                </span>
                                                            </div>
                                                        </td>
                                                        <td className="h-px w-px whitespace-nowrap">
                                                            <div className="px-6 py-1.5">
                                                                <Link
                                                                    className="mr-4 inline-flex items-center gap-x-1 text-sm text-blue-600 decoration-2 hover:underline font-medium dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                                                                    href={route('kelola.anggota', {
                                                                        id: item?.id,
                                                                        type: "update"
                                                                    })}
                                                                >
                                                                    Update
                                                                </Link>
                                                                <Link
                                                                    className="inline-flex items-center gap-x-1 text-sm text-red-600 decoration-2 hover:underline font-medium dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                                                                    href={route('kelola.anggota', {
                                                                        id: item?.id,
                                                                        type: "delete"
                                                                    })}
                                                                >
                                                                    Delete
                                                                </Link>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            }) : null
                                        }
                                    </tbody>
                                </table>
                                <div className="px-6 py-4 grid gap-3 md:flex md:justify-between md:items-center border-t border-gray-200 dark:border-gray-700">
                                    <div>
                                        <p className="text-sm text-gray-600 dark:text-gray-400">
                                            Total
                                            {" "}
                                            <span className="font-bold text-gray-800 dark:text-gray-200">
                                                {data?.length}
                                            </span>
                                            {" "}
                                            results
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default MasterTableComp