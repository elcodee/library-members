<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;


class GroupsControl extends Controller
{
    public function master_anggota(Request $request)
    {
        $sess = Auth::user();
        
        // Check Role if Admin or Member
        if($sess->role === "Admin"){
            // Get All Member Data Sort From Newest
            $user = User::orderBy("id", 'DESC')->get();
            return Inertia::render('Groups', [
                'session' => $request->session()->get('msg'),
                'user' => $user
            ]);
        } else {
            // Get All Member Data Sort From Newest
            $user = User::orderBy("id", 'DESC')->get();
            return Inertia::render('Members', [
                'session' => $request->session()->get('msg'),
                'user' => $user
            ]);
        }

    }
    
    
    public function kelola_anggota($type, $id = null)
    {
        // Check type if Detail / Update / Delete
        if($type === "create"){
            return Inertia::render('Create', []);
        } else if ($type === "update"){
            // Filter Member By ID
            $user = User::find($id);
            return Inertia::render('Update', [
                'user' => $user,
            ]);
        } else if($type === "delete"){
            // Filter Member By ID
            $user = User::find($id);
            return Inertia::render('Delete', [
                'user' => $user,
            ]);
        } else {
            // Filter Member By ID
            $user = User::find($id);
            return Inertia::render('Detail', [
                'user' => $user,
            ]);
        }
    }


    public function create_anggota_process(Request $request)
    {
        //  Data Validation
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|string|lowercase|email|max:50|unique:'.User::class,
            'phone_number' => 'required|string|max:14|unique:'.User::class,
            'address' => 'required|string|unique:'.User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        // Store To Database
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'role' => $request->role,
            'address' => $request->address,
            'password' => Hash::make($request->password),
        ]);

        // Redirect When Data Stored
        return redirect()->route('master.anggota')->with('msg', 'user_created');
    }


    public function update_anggota_process(Request $request, $id)
    {
        // dd($request, $id);

        //  Data Validation
        $updt_user = User::find($id);
        
        // Update User Data
        $updt_user->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'role' => $request->role,
            'address' => $request->address,
        ]);
        
        // Redirect When Data Updated
        return redirect()->route('master.anggota')->with('msg', 'user_updated');
    }

    
    public function delete_anggota_process($id)
    {
        //  Data Validation
        $del_user = User::find($id);
        
        // Deleted User Data
        $del_user->delete();
        
        // Redirect When Data Stored
        return redirect()->route('master.anggota')->with('msg', 'user_deleted');
    }
}
