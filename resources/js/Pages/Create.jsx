import FormAddMemberComp from '@/CustomComponents/FormAddMemberComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Create({ auth }) {
    return (
        <>
            <Head title="Tambah Anggota Perpustakaan" />
            <NavbarLayout auth={auth.user} />

            <FormAddMemberComp />
        </>
    );
}
