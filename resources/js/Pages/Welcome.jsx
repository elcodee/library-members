import HeroComp from '@/CustomComponents/HeroComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Welcome({ auth, canLogin, canRegister }) {
    return (
        <>
            <Head title="Landing" />
            <NavbarLayout auth={auth.user} />

            <HeroComp auth={auth.user} />
        </>
    );
}
