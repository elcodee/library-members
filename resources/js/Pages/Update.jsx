import FormUpdateGroupComp from '@/CustomComponents/FormUpdateMemberComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Update({ auth, user }) {
    return (
        <>
            <Head title="Edit Anggota Perpustakaan" />
            <NavbarLayout auth={auth.user} />

            <FormUpdateGroupComp user={user} />
        </>
    );
}
