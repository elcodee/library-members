import InputError from '@/Components/InputError';
import { Link, useForm } from '@inertiajs/react'
import React, { useEffect } from 'react'

const FormAddMemberComp = () => {
    const { data, setData, post, processing, errors, reset } = useForm({
        name: '',
        email: '',
        phone_number: '',
        address: '',
        role: '',
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);
    return (
        <>
            <div className="max-w-[85rem] px-4 py-4 sm:px-6 lg:px-8 lg:py-14 mx-auto">
                <div className="mx-auto max-w-2xl">
                    <div className="text-center">
                        <h2 className="text-xl text-gray-800 font-bold sm:text-3xl dark:text-white">
                            Add Member
                        </h2>
                    </div>

                    <div className="mt-5 p-4 relative z-10 bg-white border rounded-xl sm:mt-10 md:p-10 dark:bg-gray-800 dark:border-gray-700">
                        <form>
                            <div className="mb-4 sm:mb-8">
                                <label
                                    htmlFor="hs-feedback-post-comment-name-1"
                                    className="block mb-2 text-sm font-medium dark:text-white"
                                >
                                    Name
                                </label>
                                <input
                                    required
                                    value={data.name}
                                    onChange={(e) => setData('name', e.target.value)}
                                    type="text"
                                    id="hs-feedback-post-comment-name-1"
                                    className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                    placeholder="name"
                                />

                                <InputError message={errors.name} className="mt-2" />
                            </div>
                            <div className="mb-4 sm:mb-8">
                                <label
                                    htmlFor="hs-feedback-post-comment-email-1"
                                    className="block mb-2 text-sm font-medium dark:text-white"
                                >
                                    Email
                                </label>
                                <input
                                    required
                                    value={data.email}
                                    onChange={(e) => setData('email', e.target.value)}
                                    type="email"
                                    id="hs-feedback-post-comment-email-1"
                                    className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                    placeholder="email"
                                />

                                <InputError message={errors.email} className="mt-2" />
                            </div>
                            <div className="mb-4 sm:mb-8">
                                <label
                                    htmlFor="hs-feedback-post-comment-email-1"
                                    className="block mb-2 text-sm font-medium dark:text-white"
                                >
                                    Phone Number
                                </label>
                                <input
                                    required
                                    value={data.phone_number}
                                    onChange={(e) => setData('phone_number', e.target.value)}
                                    type="email"
                                    id="hs-feedback-post-comment-email-1"
                                    className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                    placeholder="phone number"
                                />

                                <InputError message={errors.phone_number} className="mt-2" />
                            </div>
                            <div className="mb-4 sm:mb-8">
                                <label
                                    htmlFor="hs-feedback-post-comment-email-1"
                                    className="block mb-2 text-sm font-medium dark:text-white"
                                >
                                    Address
                                </label>
                                <textarea
                                    required
                                    value={data.address}
                                    onChange={(e) => setData('address', e.target.value)}
                                    id="hs-feedback-post-comment-textarea-1"
                                    name="hs-feedback-post-comment-textarea-1"
                                    rows={3}
                                    className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                    placeholder="address"
                                />

                                <InputError message={errors.address} className="mt-2" />
                            </div>
                            <div className="-mt-4 sm:mb-8">
                                <label
                                    htmlFor="af-submit-app-category"
                                    className="inline-block text-sm font-medium text-gray-800 mt-2.5 dark:text-gray-200"
                                >
                                    Role
                                </label>
                                <select
                                    onChange={(e) => setData('role', e.target.value)}
                                    value={data?.role}
                                    className="py-2 px-3 pe-9 block w-full border-gray-200 shadow-sm rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                >
                                    <option selected value="">Select role</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Member">Member</option>
                                </select>

                                <InputError message={errors.role} className="mt-2" />
                            </div>
                            <div className="mb-4 sm:mb-8">
                                <label
                                    htmlFor="hs-feedback-post-comment-email-1"
                                    className="block mb-2 text-sm font-medium dark:text-white"
                                >
                                    Password
                                </label>
                                <input
                                    required
                                    value={data.password}
                                    onChange={(e) => setData('password', e.target.value)}
                                    type="text"
                                    id="hs-feedback-post-comment-email-1"
                                    className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                    placeholder="password"
                                />

                                <InputError message={errors.password} className="mt-2" />
                            </div>
                            <div className="mb-4 sm:mb-8">
                                <label
                                    htmlFor="hs-feedback-post-comment-email-1"
                                    className="block mb-2 text-sm font-medium dark:text-white"
                                >
                                    Confirm Password
                                </label>
                                <input
                                    required
                                    value={data.password_confirmation}
                                    onChange={(e) => setData('password_confirmation', e.target.value)}
                                    type="password"
                                    id="hs-feedback-post-comment-email-1"
                                    className="py-3 px-4 block w-full border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600"
                                    placeholder="confirm password"
                                />

                                <InputError message={errors.password_confirmation} className="mt-2" />
                            </div>


                            <div className="mt-6 grid">
                                <button
                                    disabled={processing}
                                    onClick={() => post(route('tambah.anggota.process'))}
                                    className="w-full py-2 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                                >
                                    Add
                                </button>
                                <Link
                                    disabled={processing}
                                    href={route('master.anggota')}
                                    className="w-full mt-4 py-1 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-gray-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600"
                                >
                                    back
                                </Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FormAddMemberComp