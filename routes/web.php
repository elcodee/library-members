<?php

use App\Http\Controllers\GroupsControl;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
    ]);
})->name('landing');


Route::middleware('auth')->prefix('anggota')->group(function () {
    // Master Table Anggota
    Route::get('/', [GroupsControl::class, 'master_anggota'])->name('master.anggota');

    // Add Member
    // Route::get('/create', [GroupsControl::class, 'create_anggota'])->name('tambah.anggota');
    Route::post('/create', [GroupsControl::class, 'create_anggota_process'])->name('tambah.anggota.process');
    
    // Detail, Update & Delete Member
    Route::get('/{type?}/{id?}', [GroupsControl::class, 'kelola_anggota'])->name('kelola.anggota');
    Route::patch('/{id?}/update', [GroupsControl::class, 'update_anggota_process'])->name('update.anggota.process');
    Route::delete('/{id?}/delete', [GroupsControl::class, 'delete_anggota_process'])->name('delete.anggota.process');
});











Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
