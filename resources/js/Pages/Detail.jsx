import FormDetailMemberComp from '@/CustomComponents/FormDetailMemberComp';
import MasterTableComp from '@/CustomComponents/MasterTableComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Detail({ auth, user }) {
    return (
        <>
            <Head title="Detail Anggota Perpustakaan" />
            <NavbarLayout auth={auth.user} />

            <FormDetailMemberComp data={user} />
        </>
    );
}
