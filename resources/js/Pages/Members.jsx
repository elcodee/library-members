import MemberListComp from '@/CustomComponents/MemberListComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Members({ auth, user }) {
    return (
        <>
            <Head title="Anggota Perpustakaan" />
            <NavbarLayout auth={auth.user} />

            <MemberListComp data={user} />
        </>
    );
}
