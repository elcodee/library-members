import React from 'react'

const MemberListComp = ({ data }) => {
    return (
        <>
            <div className="max-w-[85rem] px-4 py-10 sm:px-6 lg:px-8 lg:py-14 mx-auto">
                <div className="max-w-2xl mx-auto text-center mb-10 lg:mb-14">
                    <h2 className="text-2xl font-bold md:text-4xl md:leading-tight dark:text-white">
                        Daftar Anggota Perpustakaan
                    </h2>
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
                    {
                        data?.length > 0 ? data.map((item, key) => {
                            return (
                                <div key={key + 1} className="flex flex-col rounded-xl p-4 md:p-6 bg-white border border-gray-200 dark:bg-slate-900 dark:border-gray-700">
                                    <div className="flex items-center gap-x-4">
                                        <img
                                            className="rounded-full size-20"
                                            src="https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=facearea&facepad=2&w=900&h=900&q=80"
                                            alt="Image Description"
                                        />
                                        <div className="grow">
                                            <h3 className="font-bold text-xl text-gray-800 dark:text-gray-200">
                                                {item?.name}
                                            </h3>
                                            <p className="text-xs uppercase text-gray-500">{item?.role}</p>
                                        </div>
                                    </div>
                                    <p className="mt-3 text-gray-500">
                                       {item?.phone_number}
                                    </p>
                                    <p className="mt-0 text-gray-500">
                                       {item?.address}
                                    </p>
                                </div>
                            )
                        }) : null
                    }
                </div>
            </div>
        </>
    )
}

export default MemberListComp