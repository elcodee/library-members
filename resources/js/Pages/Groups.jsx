import MasterTableComp from '@/CustomComponents/MasterTableComp';
import NavbarLayout from '@/Layouts/NavbarLayout';
import { Head } from '@inertiajs/react';

export default function Groups({ auth, session, user }) {
    return (
        <>
            <Head title="Anggota Perpustakaan" />
            <NavbarLayout auth={auth.user} />

            <MasterTableComp session={session} data={user} />
        </>
    );
}
